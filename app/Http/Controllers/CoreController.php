<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CoreController extends Controller
{
    public function index()
    {
        return view('index');
    }

    //  private function

    public function corel(Request $request)
    {
        //  dd($request);
        $request->validate([
        'x.*' => 'required|numeric',
        'y.*' => 'required|numeric',
        ]);
        $y=$request->y;
        $x=$request->x;
        $n=count($x);
        $total_xy=0;
        $total_xx=0;
        $total_yy=0;
        $r=0;

        $total_x=array_sum($x);
        $total_y=array_sum($y);

        for ($i=0; $i < $n; $i++) {
            $total_xy+=$x[$i]*$y[$i];
            $total_xx+=pow($x[$i], 2);
            $total_yy+=pow($y[$i], 2);
        }
        $denominator=sqrt((($n*$total_xx)-(pow($total_x, 2)))*(($n*$total_yy)-(pow($total_y, 2))));
        $numerator=($n*$total_xy)-($total_x*$total_y);
        if ($denominator !=0) {
            $r=$numerator/$denominator;
            return json_encode(['corel'=>$r]);
        } else {
            return json_encode(['error'=>1]);
        }
    }

    public function LinearRegression(Request $request)
    {
        //  dd($request);
        $request->validate([
      'x.*' => 'required|numeric',
      'y.*' => 'required|numeric',
      ]);
        $y=$request->y;
        $x=$request->x;
        $n=count($x);
        $total_xy=0;
        $total_xx=0;
        $a=0;
        $b=0;

        $total_x=array_sum($x);
        $total_y=array_sum($y);

        for ($i=0; $i < $n; $i++) {
            $total_xy+=$x[$i]*$y[$i];
            $total_xx+=pow($x[$i], 2);
        }

        $a=(($total_y*$total_xx)-($total_x*$total_xy))/(($n*$total_xx)-pow($total_x, 2));
        $b=(($n*$total_xy)-($total_x*$total_y))/(($n*$total_xx)-(pow($total_x, 2)));

        return json_encode(['a'=>$a,'b'=>$b]);
    }

    public function MultipleRegression(Request $request)
    {//define function that will calculate b for each set of x or x1,x2
        $request->validate([
    'mrx.*' => 'required|numeric',
    'mrx1.*' => 'required|numeric',
    'mry.*' => 'required|numeric',
    ]);
        $y=$request->mry;
        $x1=$request->mrx;
        $x2=$request->mrx1;
        $n=count($x1);
        $ryx1=$this->correlation($x1, $y);
        $ryx2=$this->correlation($x2, $y);
        $rx1x2=$this->correlation($x1, $x2);
        $sdy=$this->std($y);
        $sdx1=$this->std($x1);
        $sdx2=$this->std($x2);
        $a=0;
        $b1=0;
        $b2=0;

        $b1=(($ryx1-($ryx2*$rx1x2))/(1-(pow($rx1x2, 2))))*(($sdy/$sdx1));
        $b2=(($ryx2-($ryx1*$rx1x2))/(1-(pow($rx1x2, 2))))*(($sdy/$sdx2));
        $a=$this->mean($y)-($b1*($this->mean($x1)))-($b2*($this->mean($x2)));


        return json_encode(['b1'=>$b1,'b2'=>$b2,'a'=>$a]);
    }

    private function correlation($x, $y)
    {
        $y=$y;
        $x=$x;
        $n=count($x);
        $total_xy=0;
        $total_xx=0;
        $total_yy=0;
        $r=0;

        $total_x=array_sum($x);
        $total_y=array_sum($y);

        for ($i=0; $i < $n; $i++) {
            $total_xy+=$x[$i]*$y[$i];
            $total_xx+=pow($x[$i], 2);
            $total_yy+=pow($y[$i], 2);
        }
        $denominator=sqrt((($n*$total_xx)-(pow($total_x, 2)))*(($n*$total_yy)-(pow($total_y, 2))));
        $numerator=($n*$total_xy)-($total_x*$total_y);
        if ($denominator !=0) {
            $r=$numerator/$denominator;
            return $r;
        } else {
            return 0;
        }
    }

    private function std($set)
    {//this is correct
        $n=count($set);
        $mean=array_sum($set)/$n;
        $numerator=0;
        for ($i=0; $i <$n ; $i++) {
            $numerator+=pow(($set[$i]-$mean), 2);
        }
        return  sqrt(($numerator/($n-1)));
    }

    private function mean($set){
      return array_sum($set)/count($set);
    }
}
