<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Prediction Model | Business Analytics</title>

  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
  <div class="">
    <div class="container">
      <div class="">
        <div class="jumbotron">
          <h3>Business Analytics</h3>
          <small>Prediction Model</small>
          <hr>
          <p>Please insert your dataset then choose operation:</p>
          <a href="#" class="btn btn-info" id="Corel">Correlation</a>
          <a href="#" class="btn btn-dark" id="LinReg">Linear Regession</a>
          <a href="#" class="btn btn-success" id="MLReg">Multiple Linear Regession</a>

        </div>
        <div id="exTab2" class="container">
          <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link active" href="#1" data-toggle="tab">Correlation /Linear Regression</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#3" data-toggle="tab">Multiple Linear Reression</a>
            </li>
          </ul>

          <div class="tab-content ">
            <div class="tab-pane active" id="1">
              <div class="row">
                <div class="col-md-6" style="margin-top:40px;">
                  <form class="form-group" action="#" method="post" id="varlist">
                    {{csrf_field()}}
                    <table class="table table-bordered" id="varstable">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">X</th>
                          <th scope="col">Y</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><input class="form-control" type="text" name="x[]" value="" id="x.0"></td>
                          <td><input class="form-control" type="text" name="y[]" value="" id="y.0"></td>
                        </tr>
                        <tr>
                          <td><input class="form-control" type="text" name="x[]" value="" id="x.1"></td>
                          <td><input class="form-control" type="text" name="y[]" value="" id="y.1"></td>
                        </tr>
                        <tr id="cloneMerow">
                          <td><input class="form-control" type="text" name="x[]" value="" id="x.2"></td>
                          <td><input class="form-control" type="text" name="y[]" value="" id="y.2"></td>
                        </tr>
                      </tbody>
                    </table>
                  </form>
                </div>
                <div class="col-md-6" style="margin-top:40px;">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="card text-white bg-info mb-3" style="max-width: 20rem;height: 239px;">
                        <div class="card-header">Correlation</div>
                        <div class="card-body">
                          <h4 class="card-title" id="corelVal"></h4>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="card text-white bg-dark mb-3" style="max-width: 20rem;min-height: 239px;">
                        <div class="card-header">Linear Regression</div>
                        <div class="card-body">
                          <strong style="display:none;">Model:</strong>
                          <h4 class="card-title" id="linearVal"></h4>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <p style="margin-left:10px;" class="text-info">Add Row <a style="margin-left:10px;" href="#" id="addfields" class="btn btn-sm btn-outline-dark fa fa-plus"></a></p>
            </div>
            <div class="tab-pane" id="3">
              <div class="row">
                <div class="col-md-6" style="margin-top:40px;">
                  <form class="form-group" action="#" method="post" id="varlistMR">
                    {{csrf_field()}}
                    <table class="table table-bordered" id="varstableMR">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">X1</th>
                          <th scope="col">X2</th>
                          <th scope="col">Y</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><input class="form-control" type="text" name="mrx[]" value="" id="mrx.0"></td>
                          <td><input class="form-control" type="text" name="mrx1[]" value="" id="mrx1.0"></td>
                          <td><input class="form-control" type="text" name="mry[]" value="" id="mry.0"></td>
                        </tr>
                        <tr>
                          <td><input class="form-control" type="text" name="mrx[]" value="" id="mrx.1"></td>
                          <td><input class="form-control" type="text" name="mrx1[]" value="" id="mrx1.1"></td>
                          <td><input class="form-control" type="text" name="mry[]" value="" id="mry.1"></td>
                        </tr>
                        <tr id="cloneMerowMR">
                          <td><input class="form-control" type="text" name="mrx[]" value="" id="mrx.2"></td>
                          <td><input class="form-control" type="text" name="mrx1[]" value="" id="mrx1.2"></td>
                          <td><input class="form-control" type="text" name="mry[]" value="" id="mry.2"></td>
                        </tr>
                      </tbody>
                    </table>
                  </form>
                </div>
                <div class="col-md-6" style="margin-top:40px;">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="card text-white bg-success mb-3" style=" min-height: 239px;">
                        <div class="card-header">Multiple Linear Regression</div>
                        <div class="card-body">
                          <strong style="display:none;">Model:</strong>
                          <h4 class="card-title" id="MulVal"></h4>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <p style="margin-left:10px;" class="text-info">Add Row <a style="margin-left:10px;" href="#" id="addfieldsMR" class="btn btn-sm btn-outline-dark fa fa-plus"></a></p>
            </div>
          </div>

        </div>
      </div>

    </div>
  </div>

  {{-- error modal --}}
  <div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-danger"><span class="fa fa-thumbs-down"></span> Error</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <p></p>
        </div>
      </div>
    </div>
  </div>
  {{-- end of error modal --}}

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      var idc = 3;
      $('#addfields').click(function() {
        var count = 1;
        var $clone = $('#cloneMerow').clone();
        $clone.attr('id', $clone.attr('id').replace(/\d+$/, function(str) {
          return parseInt(str) + 1;
        }));
        // Find all elements in $clone that have an ID, and iterate using each()
        $clone.find('[id]').each(function() {

          //Perform the same replace as above
          var $th = $(this);
          var newID = $th.attr('id').replace(/\d+$/, function(str) {
            return parseInt(str) + 1;
          });
          $th.attr('id', newID);
        });
        $("#cloneMerow").removeAttr('id');
        $clone.appendTo('#varstable');
      });

      $('#addfieldsMR').click(function() {
        var count = 1;
        var $clone = $('#cloneMerowMR').clone();
        $clone.attr('id', $clone.attr('id').replace(/\d+$/, function(str) {
          return parseInt(str) + 1;
        }));
        // Find all elements in $clone that have an ID, and iterate using each()
        $clone.find('[id]').each(function() {

          //Perform the same replace as above
          var $th = $(this);
          var newID = $th.attr('id').replace(/\d+$/, function(str) {
            return parseInt(str) + 1;
          });
          $th.attr('id', newID);
        });
        $("#cloneMerowMR").removeAttr('id');
        $clone.appendTo('#varstableMR');
      });

      var x;

      $('#Corel').click(function() {
        $(':input').removeClass('has-error');
        $.ajax({
          type: "post",
          url: "{{route('corel')}}",
          cache: false,
          data: $('#varlist').serialize(),
          success: function(json) {
            try {
              var obj = jQuery.parseJSON(json);
              if (obj['error']) {
                $('#corelVal').text("Division By Zero Not Allowed");
              } else {
                $('#corelVal').text("");
                $('#corelVal').text(obj['corel']);
              }
            } catch (e) {
              alert('error parsing result');
            }
          },
          error: function(json) {
            $('.modal-body>p').text('');
            $('.modal-body>p').text(json['responseJSON']['message'] + " Please check the marked fields.");
            for (var e in json['responseJSON']['errors']) {
              console.log(e);
              x = document.getElementById(e);
              x.classList.add("has-error");
              console.log(json['responseJSON']['errors'][e]);
            }
            $('.modal').modal('show');
          }
        });

      });
      // end corel of AJAX

      // start of linear reg AJAX
      $('#LinReg').click(function() {
        $(':input').removeClass('has-error');
        $.ajax({
          type: "post",
          url: "{{route('linear')}}",
          cache: false,
          data: $('#varlist').serialize(),
          success: function(json) {
            try {
              var obj = jQuery.parseJSON(json);
              if (obj['error']) {
                $('#linearVal').text("Division By Zero Not Allowed");
              } else {
                $('.card-body>strong').show();
                $('#linearVal').text("");
                $('#linearVal').html("a=" + obj['a'] + "  " + "b=" + obj['b'] + "<br>" + "y=" + obj['a'] + "+" + obj['b'] + "⋅x");
              }
            } catch (e) {
              alert('error parsing result');
            }
          },
          error: function(json) {
            $('.modal-body>p').text('');
            $('.modal-body>p').text(json['responseJSON']['message'] + " Please check the marked fields.");
            for (var e in json['responseJSON']['errors']) {
              console.log(e);
              x = document.getElementById(e);
              x.classList.add("has-error");
              console.log(json['responseJSON']['errors'][e]);
            }
            $('.modal').modal('show');
          }
        });

      });
      // end of Linear reg AJAX

      // start of Multi linear reg AJAX
      $('#MLReg').click(function() {
        $(':input').removeClass('has-error');
        $.ajax({
          type: "post",
          url: "{{route('MulReg')}}",
          cache: false,
          data: $('#varlistMR').serialize(),
          success: function(json) {
            try {
              alert(json);
              var obj = jQuery.parseJSON(json);
              if (obj['error']) {
                $('#MulVal').text("Division By Zero Not Allowed");
              } else {
                $('.card-body>strong').show();
                $('#MulVal').text("");
                $('#MulVal').html("y=" + obj['a'] + "+" + obj['b1'] + " <b>x1</b> " + obj['b2'] + " <b>x2</b>");
              }
            } catch (e) {
              alert('error parsing result');
            }
          },
          error: function(json) {
            $('.modal-body>p').text('');
            $('.modal-body>p').text(json['responseJSON']['message'] + " Please check the marked fields.");
            for (var e in json['responseJSON']['errors']) {
              console.log(e);
              x = document.getElementById(e);
              x.classList.add("has-error");
              console.log(json['responseJSON']['errors'][e]);
            }
            $('.modal').modal('show');
          }
        });

      });
      // end of Multi Linear reg AJAX
    });
  </script>
  <style media="screen">
    .has-error {
      border: red 1px solid;
    }

    #exTab1 .tab-content {
      color: white;
      background-color: #428bca;
      padding: 5px 15px;
    }

    #exTab2 h3 {
      color: white;
      background-color: #428bca;
      padding: 5px 15px;
    }

    /* remove border radius for the tab */

    #exTab1 .nav-pills>li>a {
      border-radius: 0;
    }

    /* change border radius for the tab , apply corners on top*/

    #exTab3 .nav-pills>li>a {
      border-radius: 4px 4px 0 0;
    }

    #exTab3 .tab-content {
      color: white;
      background-color: #428bca;
      padding: 5px 15px;
    }
  </style>
</body>

</html>
