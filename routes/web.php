<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CoreController@index');
Route::post('corel', 'CoreController@Corel')->name('corel');
Route::post('linear','CoreController@LinearRegression')->name('linear');
Route::post('multiple', 'CoreController@MultipleRegression')->name('MulReg');
